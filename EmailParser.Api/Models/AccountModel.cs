﻿using System.Xml;
using System.Xml.Serialization;

namespace EmailParser.Api.Models
{
    [XmlRoot("account")]
    public class AccountModel
    {
        [XmlElement("vendor")]
        public string Vendor { get; set; }

        [XmlElement("description")]
        public string Description { get; set; }

        [XmlElement("date")]
        public string Date { get; set; }
        
        [XmlElement("expense")]
        public ExpenseModel Expense { get; set; }
    }
}
