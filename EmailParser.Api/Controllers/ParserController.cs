﻿using EmailParser.Api.Models;
using EmailParser.Api.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace EmailParser.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ParserController : ControllerBase
    {
        private readonly IParserRepository _repository;

        public ParserController(IParserRepository repository)
            : base()
        {
            _repository = repository;
        }

        [HttpGet]
        public ActionResult<string> Get()
        {
            bool isInitialised = _repository != null;

            return isInitialised ? "Api initialised..." : "Api not initialised...";
        }

        [HttpPost("account")]
        public ActionResult<AccountModel> GetAccount([FromBody] string inputData)
        {
            return _repository.GetAccount(inputData);
        }
    }
}
