﻿using EmailParser.Api.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace EmailParser.Api.Utilities
{
    public static class XmlDocumentExtension
    {
        public static AccountModel ToAccountModel(this XmlDocument xDoc)
        {
            if (xDoc.GetElementsByTagName("total").Count == 0)
            {
                throw new KeyNotFoundException("No totals node found in the Xml document.");
            }

            AccountModel result = null;

            using (var stream = new MemoryStream())
            {
                xDoc.Save(stream);

                stream.Position = 0;

                var serialiser = new XmlSerializer(typeof(AccountModel));
                result = (AccountModel)serialiser.Deserialize(stream);
            }

            return result;
        }
    }
}
