﻿using System;
using System.Xml.Serialization;

namespace EmailParser.Api.Models
{
    [XmlRoot("expense")]
    public class ExpenseModel
    {
        [XmlElement(ElementName = "cost_centre", IsNullable = true)]
        public string CostCentre { get; set; } = "UNKNOWN";

        [XmlElement("payment_method")]
        public string PaymentMethod { get; set; }

        [XmlElement("total")]
        public double Total { get; set; }

        public double Gst
        {
            get
            {
                return Math.Round(Total * 0.15, 2);
            }
        }
        public double Gross
        {
            get
            {
                return Math.Round(Total - Gst, 2);
            }
        }
    }
}
