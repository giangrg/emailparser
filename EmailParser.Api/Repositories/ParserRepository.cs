﻿using EmailParser.Api.Models;
using EmailParser.Api.Utilities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;

namespace EmailParser.Api.Repositories
{
    public class ParserRepository : IParserRepository
    {
        private readonly string _emailRegex;
        
        public ParserRepository()
        {
            // Note: This is the suggested regex pattern based on RFC 2822
            _emailRegex = @"<[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?>";
        }

        public AccountModel GetAccount(string inputData)
        {
            if (string.IsNullOrEmpty(inputData))
            {
                throw new ArgumentNullException("inputData", "Parameter cannot be a null or empty string");
            }

            var xmlData = ExtractValidXmlData(inputData);
            
            if (xmlData == null)
            {
                throw new ArgumentNullException("inputData", "Invalid xml string");
            }

            return xmlData.ToAccountModel();
        }

        private XmlDocument ExtractValidXmlData(string inputData)
        {
            var isValidXml = true;
            var result = new XmlDocument();
            var root = result.CreateElement("account");
            result.AppendChild(root);

            var sanitisedData = SanitiseInputData(inputData);
            var xmlString = $"<root>{sanitisedData}</root>";

            using (var stream = new MemoryStream(Encoding.UTF8.GetBytes(xmlString)))
            {
                var readerSettings = new XmlReaderSettings()
                {
                    IgnoreWhitespace = true,
                    CheckCharacters = true,
                    Async = true
                };

                using (var reader = XmlReader.Create(stream, readerSettings))
                {
                    try
                    {
                        var documentNode = result.ReadNode(reader);

                        if (documentNode.HasChildNodes)
                        {
                            for (var i = 0; i < documentNode.ChildNodes.Count; i++)
                            {
                                var currentNode = documentNode.ChildNodes.Item(i);

                                if (currentNode.NodeType == XmlNodeType.Element)
                                {
                                    result.DocumentElement.AppendChild(currentNode);
                                }
                            }
                        }
                    }
                    catch
                    {
                        // skip the rest of the message if it encounters an invalid xml node
                        reader.Skip();
                        isValidXml = false;
                    }
                }
            }

            return isValidXml ? result : null;
        }

        private string SanitiseInputData(string inputData)
        {
            return Regex.Replace(inputData, _emailRegex, "", RegexOptions.IgnoreCase);
        }
    }
}
