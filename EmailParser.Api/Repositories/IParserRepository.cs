﻿using EmailParser.Api.Models;

namespace EmailParser.Api.Repositories
{
    public interface IParserRepository
    {
        AccountModel GetAccount(string inputData);
    }
}
