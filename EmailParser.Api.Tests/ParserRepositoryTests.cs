using EmailParser.Api.Repositories;
using System;
using System.Text;
using Xunit;

namespace EmailParser.Api.Tests
{
    public class ParserRepositoryTests
    {
        private IParserRepository _repository;

        public ParserRepositoryTests()
        {
            _repository = new ParserRepository();
        }

        [Fact]
        public void GetAccount_WithValidInput_ReturnsCorrectResults()
        {
            var result = _repository.GetAccount(GetTestEmailString());

            Assert.NotNull(result);
            Assert.NotNull(result.Expense);
            Assert.Equal(1024.01, result.Expense.Total);
        }

        [Fact]
        public void GetAccount_WithValidInput_ReturnsCorrectGst()
        {
            var result = _repository.GetAccount(GetTestEmailString());

            Assert.NotNull(result);
            Assert.NotNull(result.Expense);

            var expected = Math.Round(result.Expense.Total - result.Expense.Gross, 2);
            Assert.Equal(expected, result.Expense.Gst);
        }

        [Fact]
        public void GetAccount_WithValidInput_ReturnsCorrectGross()
        {
            var result = _repository.GetAccount(GetTestEmailString());

            Assert.NotNull(result);
            Assert.NotNull(result.Expense);

            var expected = Math.Round(result.Expense.Total - result.Expense.Gst, 2);
            Assert.Equal(expected, result.Expense.Gross);
        }

        [Fact]
        public void GetAccount_WithInvalidInput_ThrowsException()
        {
            Assert.Throws<ArgumentNullException>(() => _repository.GetAccount(""));
        }

        #region Miscellaneous

        private string GetTestEmailString()
        {
            var result = new StringBuilder();

            result.AppendLine("Hi Yvaine,");
            result.AppendLine("Please create an expense claim for the below. Relevant details are marked up as requested�");
            result.AppendLine("<expense><cost_centre>DEV002</cost_centre><total>1024.01</total><payment_method>personal card</payment_method></expense>");
            result.AppendLine("From: Ivan Castle Sent: Friday, 16 February 2018 10:32 AM To: Antoine Lloyd <Antoine.Lloyd@example.com> Subject: test");
            result.AppendLine("Hi Antoine,");
            result.AppendLine("Please create a reservation at the <vendor>Viaduct Steakhouse</vendor> our <description>development team�s project end celebration dinner</description> on <date>Tuesday 27 April 2017</date>. We expect to arrive around 7.15pm. Approximately 12 people but I�ll confirm exact numbers closer to the day.");
            result.AppendLine("Regards,");
            result.AppendLine("Ivan");

            return result.ToString();
        }

        #endregion Miscellaneous
    }
}
