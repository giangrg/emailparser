﻿const apiRoute = "api/parser";

function processAccount() {
    const inputData = $("#email-data-textarea").val();

    $.ajax({
        type: "POST",
        accepts: "application/json",
        url: apiRoute + "/account/",
        contentType: "application/json",
        data: JSON.stringify(inputData),
        error: function (jqXHR, textStatus, errorThrown) {
            console.log("An error has occured");
        },
        success: function (result) {
            displayResults(result);
        }
    });
}

function displayResults(data) {
    $("input[id='account-vendor']").val(data["vendor"]);
    $("input[id='account-description']").val(data["description"]);
    $("input[id='account-date']").val(data["date"]);

    let expenseData = data["expense"];
    $("input[id='account-costcentre']").val(expenseData["costCentre"]);
    $("input[id='account-paymethod']").val(expenseData["paymentMethod"]);
    $("input[id='account-gross']").val(expenseData["gross"]);
    $("input[id='account-gst']").val(expenseData["gst"]);
    $("input[id='account-total']").val(expenseData["total"]);
}